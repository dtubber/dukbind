# About dukbind:

Attempt at creating a wrapper for [duktape](http://duktape.org/), a free and open-source JavaScript interpeter. 
    
Aims at making things like exposing a C++ class easier, and to keep the complex duktape API hidden from the end user.
    
dukbind includes the duktape JavaScript interpreter.

## What currently works:
    
* DukContext can evaluate JavaScript strings and files.
* DukFunction can wrap a C-function (with duktape signature)
* DukModule holds a collection of DukFunctions, which can be shared between DukContext's
* You can "expose" C-functions by adding a DukFunction to a DukContext (or DukModule, see above.)
    
## What currently doesn't work:
    
* Everything other than exposing simple C-functions :DDD
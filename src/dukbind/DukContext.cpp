#include<dukbind/DukContext.hpp>
#include<iostream>

using namespace std;
using namespace dukbind;

DukContext::DukContext()
{
    duk_context* ctx = duk_create_heap_default();
    this->m_Handle = ctx;
}
DukContext::~DukContext()
{
    duk_context* ctx = this->m_Handle;
    duk_destroy_heap(ctx);
}

duk_context* DukContext::getHandle()
{
    return this->m_Handle;
}

void DukContext::eval(std::string code_item)
{
    duk_context* ctx = this->m_Handle;
    if(duk_peval_string(ctx, code_item.c_str())!=0)
    {
        cout << "[duktape|ERROR] Could not evaluate the following statement:" << endl << "-----" << endl << code_item << endl << "-----" << endl;
        cout << "[duktape|ERROR] Error message: \"" << duk_safe_to_string(ctx, -1) << "\"";
    }
}
void DukContext::evalFile(std::string filename_item)
{
    duk_context* ctx = this->m_Handle;
    if(duk_peval_file(ctx, filename_item.c_str())!=0)
    {
        cout << "[duktape|ERROR] Could not evaluate the file \""<< filename_item <<"\"." << endl;
        cout << "[duktape|ERROR] Error message: \"" << duk_safe_to_string(ctx, -1) << "\"";
    }
}

void DukContext::add(dukbind::DukFunction& function_item)
{
    duk_context* ctx = this->m_Handle;
    duk_push_c_function(ctx, *function_item.getFunction().target<duk_ret_t(*)(duk_context*)>(), DUK_VARARGS);
    duk_put_global_string(ctx, function_item.getName().c_str());
}
void DukContext::add(dukbind::DukModule& module_item)
{
    duk_context* ctx = this->m_Handle;
    std::vector<DukFunction> function_list = module_item.getFunctions();
    for(DukFunction df : function_list)
    {
        duk_push_c_function(ctx, *df.getFunction().target<duk_ret_t(*)(duk_context*)>(), DUK_VARARGS);
        duk_put_global_string(ctx, df.getName().c_str());
    }
}


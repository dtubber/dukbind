#include<dukbind/DukModule.hpp>

using namespace dukbind;

DukModule::DukModule()
{

}
DukModule::~DukModule()
{

}

std::vector<DukFunction>& DukModule::getFunctions()
{
    return this->m_Functions;
}

void DukModule::add(DukFunction function_item)
{
    m_Functions.push_back(function_item);
}

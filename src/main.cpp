/**
Copyright (c) 2016 Daniel Wanner

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <tinydir.h>
#include <iostream>
#include <string>
#include <dukbind.hpp>
#include <sstream>



using namespace std;
using namespace dukbind;

//duktape c bound functions.
duk_ret_t c_print(duk_context* c)
{
    const char* str = duk_require_string(c, 0);
    cout << str;
    return 0;
}
duk_ret_t c_printLn(duk_context* c)
{
    const char* str = duk_require_string(c, 0);
    cout << str << endl;
    return 0;
}
duk_ret_t c_print_lel(duk_context* c)
{
    cout << "print \"lel\" request incoming from duktape. [lel]" << endl;
    return 0;
}
duk_ret_t c_print_lmao(duk_context* c)
{
    cout << "print \"lmao\" request incoming from duktape. [lmao]" << endl;
    return 0;
}
duk_ret_t c_eval_file(duk_context* c)
{
    const char* path = duk_require_string(c, 0);
    if (duk_peval_file(c, path) != 0)
    {
        printf("eval failed: %s\n", duk_safe_to_string(c, -1));
    }
    return 0;
}
duk_ret_t c_readdir(duk_context* c)
{
    const char* path = duk_require_string(c, 0);
    tinydir_dir dir;
    tinydir_open(&dir, path);

    cout << "found the following entries: ";
    int ccount = 0;
    while (dir.has_next)
    {
        if(ccount>=2) cout << endl;
        ccount++;
        tinydir_file file;
        tinydir_readfile(&dir, &file);
        string filename(file.name), fileext(file.extension), filepath(file.path);
        string dot = ".", dotdot = "..";
        if(filename == dot || filename == dotdot)
        {

        }
        else
        {
            if (file.is_dir)
            {
                cout << filepath << "/";
            }
            if (file.is_reg)
            {
                cout << filepath << ", " << fileext << "-FILE";
            }
        }
        tinydir_next(&dir);
    }
    cout << endl;
    tinydir_close(&dir);
    return 0;
}
//~duktape c bound functions.
int some(int arg1, std::string arg2)
{
    return 101;
}
int some2(int arg1, std::string arg2)
{
    return 303;
}

int main(int argc, char** argv)
{
    /**DukContext ctx;
    DukFunction f1(c_print, "c_print");
    DukFunction f2(c_printLn, "c_printLn");
    DukFunction f3(c_print_lel, "c_print_lel");
    DukFunction f4(c_print_lmao, "c_print_lmao");
    DukFunction f5(c_eval_file, "c_eval_file");
    DukFunction f6(c_readdir, "c_readdir");
    ctx.add(f1);
    ctx.add(f2);
    ctx.add(f3);
    ctx.add(f4);
    ctx.add(f5);
    ctx.add(f6);
    string line = "", exit = "exit";
    while(true)
    {
        cout << "dukbind_eval>> ";
        getline(cin,line);
        if(line == "exit")
        {
            break;
        }
        else
        {
            cout << "<-" << endl;
            ctx.eval(line); cout << endl;
            cout << "->" << endl;
        }
    }*/
    DukContext ctx;
    string s;
    DukFunction df1, df2;
    df1.bindFunction<DUK_ID>(some, "some");
    df2.bindFunction<DUK_ID>(some2, "some2");
    ctx.add(df1);
    ctx.add(df2);
    ctx.eval("some(); some2();");
    cin >> s;
    return 0;
}

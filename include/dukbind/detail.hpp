/**
Copyright (c) 2016 Daniel Wanner

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef DUKBIND_DETAIL_HPP
#define DUKBIND_DETAIL_HPP

#define DUK_ID __COUNTER__

#include<functional>
#include<tuple>
#include<utility>
#include<iostream>
#include<type_traits>

#include<duktape/duktape.h>




namespace dukbind
{
    using duk_function_t = std::function<duk_ret_t(duk_context*)>;

    namespace detail
    {
        extern void* duk_function_data;
        extern void* duk_method_data;
        extern void* duk_constructor_data;

        template<typename T>
        inline T duk_get_arg(duk_context* ctx, int i)
        {
            return *(new T());
        }

        template<>
        inline std::string duk_get_arg<std::string>(duk_context* ctx, int i)
        {
            std::string ret(duk_require_string(ctx, i));
            return ret;
        }

        template<>
        inline char* duk_get_arg<char*>(duk_context* ctx, int i)
        {
            char* ret;
            strcpy(ret, duk_require_string(ctx, i));
            return ret;
        }

        template<>
        inline int duk_get_arg<int>(duk_context* ctx, int i)
        {
            int ret = duk_require_int(ctx, i);
            return ret;
        }

        template<>
        inline unsigned int duk_get_arg<unsigned int>(duk_context* ctx, int i)
        {
            unsigned int ret = duk_require_uint(ctx, i);
            return ret;
        }

        template<>
        inline float duk_get_arg<float>(duk_context* ctx, int i)
        {
            double ret = (float)duk_require_number(ctx, i);
            return ret;
        }

        template<>
        inline double duk_get_arg<double>(duk_context* ctx, int i)
        {
            double ret = duk_require_number(ctx, i);
            return ret;
        }

        template<typename T_Return, typename ... T_Params, size_t ... T_Is>
        inline void duk_get_args_impl(duk_context* ctx, std::function<T_Return(T_Params ...)> func, std::index_sequence<T_Is ...>)
        {
            using tuple_type = std::tuple<T_Params ...>;
            func(duk_get_arg<std::tuple_element_t<T_Is, tuple_type>>(ctx, T_Is) ...);
        }

        inline void duk_get_args()
        {

        }

        template<unsigned int I_Unique, typename T_Return, typename ... T_Params>
        inline duk_ret_t duk_function_proxy(duk_context* ctx)
        {
            static bool function_ran_already = false;
            static const unsigned int function_id = I_Unique;
            static std::function<T_Return(T_Params ...)> function_item = *((std::function<T_Return(T_Params ...)>*)duk_function_data);
            if(function_ran_already)
            {
                //Parse arguments here.
                //T_Return return_item;
                //return_item =
                std::cout << "proxy function executed! my unique id is: " << function_id << std::endl;
            }
            else if (!function_ran_already)
            {
                duk_function_data = NULL;
                std::cout << "address of contained function: \"" << (void*)function_item.template target<T_Return(*)(T_Params ...)>() << "\"." << std::endl;
                function_ran_already = true;
            }
            return 0;
        }

    }

    /**template<typename T_Return, typename ... T_Params>
    inline duk_function_t duk_function(std::function<T_Return(T_Params ...)> function_item)
    {
        detail::duk_function_data = &function_item;
        detail::duk_function_proxy<unique_id, T_Return, T_Params ...>(NULL);
        duk_function_t ret(detail::duk_function_proxy<unique_id, T_Return, T_Params ...>);
        return ret;
    }*/
}
#endif // DUKBIND_DETAIL_HPP
